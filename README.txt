/* $Id */

-- SUMMARY --

The account_sync module allows you to synchronize drupal user account data across
multiple Drupal sites. It currently supports only the most basic account information
such as roles, email address, and password.

This module uses XMLRPC to transmit data between sites when updates are made so
there's no need to have your sites running on the same database, server, or on the
same subdomain.

It is not recommended to use this module in a production environment. It's still in
the early stages of development and testing and you could risk breaking your
accounts by using it.

-- INSTALLATION --

* Install the account_sync module as usual on all sites you wish to use it
   see http://drupal.org/node/70151 for further information.

 * By default all syncing settings are disabled so merely installing and enabling the
   module isn't enough to start the syncing process.

-- USAGE --

 * Configure module at http://profiletest/admin/user/account_sync

 * To enable the sending of account data /to/ third party drupal sites check "Enable account syncing"

 * To enable the receiving of account data /from/ third party drupal sites check "Allow other sites to sync..".

 * The server key setting will allow the data to be encrypted between the different Drupal sites when data is sent over XMLRPC

 * Each drupal site you wish to send data to must be listed in the "Drupal sites to sync to" textarea. Sites that wish to sync to this Drupal site will be able to do so simply by having the server key and don't need to be listed here. Possibly in the future, for added security there should also be restriction by domain on incoming data.

 * Unless you know what you're doing, DO NOT enable "Allow UID 1 to by synced" if something goes wrong you could potentially lock yourself out of all of your sites.

 * The "Attempt to match roles" setting should be enabled unless you know your role id's are all in sync. The account sync module synchronizes roles across sites and enabling this will synchronize the roles based on role name as opposed to role id.

-- TODO --

 * Add domain restrictions for incoming data sync
 * Synchronize Drupal profile data
 * Synchronize content_profile data

-- CONTACT --

For bug reports, feature suggestions and latest developments visit the
project page: http://drupal.org/project/account_sync

Maintainers:
 * Scott Hadfield (hadsie) <hadsie@gmail.com>
